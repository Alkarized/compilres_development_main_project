### var 10

``` ebnf
<Программа> ::= <Объявление переменных> <Описание вычислений> .  
<Описание вычислений> ::= [<Список операторов>]  
<Объявление переменных> ::= Var <Список переменных>  
<Список переменных> ::= <Идент> | <Идент> , <Список переменных>  
<Список операторов> ::= <Оператор> | <Оператор> <Список операторов>  
<Оператор>::=<Присваивание> |<Сложный оператор>   
<Присваивание> ::= <Идент> = <Выражение>  
<Выражение> ::= <Ун.оп.> <Подвыражение> | <Подвыражение>  
<Подвыражение> :: = ( <Выражение> ) | <Операнд> | <Подвыражение> <Бин.оп.> <Подвыражение>  
<Ун.оп.> ::= "-"|"not"  
<Бин.оп.> ::= "-" | "+" | "*" | "/" |"<"|">"|"=="   
<Операнд> ::= <Идент> | <Const>  
|<Сложный оператор>:: =<Оператор цикла>|<Составной оператор>  
<Оператор цикла>:: =WHILE <Выражение> DO <Оператор>  
<Составной оператор>::= { < Список операторов > }  
<Идент> ::= <Буква> <Идент> | <Буква>  
<Const> ::= <Цифра> <Const> | <Цифра>  
```  
tested:

- ``Var xt, xy, as [x=1].`` ok
- ``Var xt, xy, as [x=1 y=1].`` ok
- ``Var xt, xy, as [x=1 y=x].``  ok
- ``Var a,b [a=5+3 b=7+8].`` ok (also with -, /, * ok)
- ``Var a,b [a=5*3-4].`` ok
- ``Var a,b [b=3 WHILE b>1 DO b=b-1].`` ok
- ``Var a,b [a=1 b=a+1].`` ok

ccыль на эмулятор:
https://github.com/asurkis/risc-emulator/tree/main