#include <malloc.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "node.h"

static VariableList *variables_start = NULL;
static VariableList *variables_last = NULL;

struct VariableList {
    VariableList *next;
    Node *value;
};

Node *node_create(NodeType type, const char* value, Node* left, Node* right) {
    Node *node = (Node *) malloc(sizeof(Node));
    node->type = type;
    if (value)
        node->value = strdup(value);
    node->left = left;
    node->right = right;
    return node;
}

void node_free(Node *node) {
    if (node == NULL) return;
    node_free(node->left);
    node_free(node->right);
    if (node->value)
        free(node->value);
    free(node);
}

void print_tabs(FILE *file, int count) {
    for (int i = 0; i < count; i++) {
        fprintf(file, "  ");
    }
}

void _file_print_node(FILE *file, Node *node, int level) {
    fprintf(file, "type: %s", node_types[node->type]);
    if (node->value) {
        fprintf(file, "\n");
        print_tabs(file, level);
        fprintf(file, "value: %s", node->value);
    }
    if (node->left) {
        fprintf(file, "\n");
        print_tabs(file, level);
        fprintf(file, "left:\n");
        print_tabs(file, level + 1);
        _file_print_node(file, node->left, level + 1);
    }
    if (node->right) {
        fprintf(file, "\n");
        print_tabs(file, level);
        fprintf(file, "right:\n");
        print_tabs(file, level + 1);
        _file_print_node(file, node->right, level + 1);
    }
}

void file_print_node(FILE *file, Node *node) {
    _file_print_node(file, node, 0);
    fprintf(file, "\n");
}

void print_node(Node *node) {
    file_print_node(stdout, node);
	//printf("new val - %s\n", node->left->value);
}
