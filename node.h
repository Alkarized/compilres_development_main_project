#ifndef NODE_H
#define NODE_H

#include <stdio.h>

typedef enum {
	PROGRAM,
	OPERAND_IDENT,
    OPERAND_CONST,
    VARS_LIST,
    VARIABLE,
    OPERATORS_LIST,
    OPERATOR,
    ASSIGNMENT,
    EXPRESSION,
    SUBEXPRESSION,
    COMPLEX_OPERATOR,
    CYCLE_OPERATOR,
    COMPOUND_OPERATOR	
} NodeType;

static const char *node_types[] = {
	"PROGRAM",
    "OPERAND_IDENT",
    "OPERAND_CONST",
    "VARS_LIST",
    "VARIABLE",
    "OPERATORS_LIST",
    "OPERATOR",
    "ASSIGNMENT",
    "EXPRESSION",
    "SUBEXPRESSION",
    "COMPLEX_OPERATOR",
    "CYCLE_OPERATOR",
    "COMPOUND_OPERATOR"
};

typedef struct Node {
    NodeType type;
    char *value;
    struct Node *left;
    struct Node *right;
} Node;

Node *node_create(NodeType type, const char* value, Node* left, Node* right);
void node_free(Node *node);

void file_print_node(FILE *file, Node *node);
void print_node(Node *node);

struct VariableList;
typedef struct VariableList VariableList;

#endif //NODE_H