all: compile

parser.tab.c parser.tab.h: parser.y
	bison -d parser.y -Wall

lex.yy.c: lexer.l parser.tab.h
	flex lexer.l

compile: node.c node.h lex.yy.c parser.tab.c parser.tab.h
	gcc -g -I. -o main node.c lex.yy.c parser.tab.c parser.tab.h -lfl

clean:
	rm -f main lex.yy.c parser.tab.c parser.tab.h parser.output

.PHONY: rebuild

rebuild: | clean all
