import yaml
import numpy as np

vars = []
program = []

free_registers = ['x1', 'x2', 'x3', 'x4', 'x5', 'x6', 'x7', 'x8', 'x9', 'x10',
                  'x11', 'x12', 'x13', 'x14', 'x15', 'x16', 'x17', 'x18', 'x19', 'x20', 'x21',
                  'x22', 'x23', 'x24', 'x25', 'x26', 'x27', 'x28', 'x29', 'x30', 'x31', 'x32']

opcode_dict = dict([
    ('+', 'add'),
    ('-', 'sub'),
    ('*', 'mul'),
    ('/', 'div')
])

jumps_dict = dict([
    ('<', 'blt'),
    ('>', 'bge'),
    ('==', 'beq')
])

reg_var = dict()
assignments = []


def save_program():
    res = np.vstack(program)
    np.savetxt("test.s", res, delimiter=' ', newline="\n", fmt="%s")


def handle_left_program_part(left_branch):
    variable_decl = left_branch['left']
    vars.append(variable_decl['value'])
    if 'right' in left_branch:
        handle_left_program_part(left_branch['right'])
    else:
        return


def add_sequence(expression):
    seq = ''
    if len(expression) == 4:
        seq = expression[0] + ' ' + expression[1] + ',' + expression[2] + ',' + expression[3]
    if len(expression) == 3:
        seq = expression[0] + ' ' + expression[1] + ',' + str(expression[2])
    program.append(seq)


subexpression = []


def handle_subexpression(sub_exp_dict, rd, rs1, rs2):
    right_reg = 0
    if 'type' in sub_exp_dict:
        token_type = sub_exp_dict['type']
        type_val = 0
        if 'value' in sub_exp_dict:
            type_val = sub_exp_dict['value']
        if token_type == 'OPERAND_CONST':
            operation = ['li', rd, type_val]
            subexpression.append(operation)
        if type_val in opcode_dict.keys():
            operation = [opcode_dict[type_val], rd, rs1, rs2]
            right_reg = operation[3]
            subexpression.append(operation)
    if 'left' in sub_exp_dict:
        next_left = sub_exp_dict['left']
        if next_left['type'] == 'OPERAND_CONST':
            rd = rs1
        if next_left['type'] == 'SUBEXPRESSION':
            if 'value' in next_left:
                rd = rs1
                rs1 = free_registers.pop(0)
                rs2 = free_registers.pop(0)
        handle_subexpression(next_left, rd, rs1, rs2)
    if 'right' in sub_exp_dict:
        next_right = sub_exp_dict['right']
        if next_right['left']['type'] == 'OPERAND_CONST':
            rd = right_reg
            handle_subexpression(next_right['left'], rd, rs1, rs2)
        else:
            handle_subexpression(next_right, rd, rs1, rs2)


def handle_assignment_token(expr_dict, expression):
    val = expr_dict['value']
    pass
    if val in reg_var.keys():
        dest_reg = reg_var[val]
    else:
        dest_reg = free_registers.pop(0)
    next_subexpr = expr_dict['left']['left']
    source_reg = None
    is_ident = False
    if next_subexpr['type'] == 'SUBEXPRESSION':
        tok = next_subexpr['left']['left']
        if tok['type'] == 'OPERAND_IDENT':
            is_ident = True
            source_reg = reg_var[tok['value']]
    if not is_ident:
        source_reg = free_registers.pop(0)
    handle_subexpression(next_subexpr, dest_reg, source_reg, free_registers.pop(0))
    expression.append(subexpression)
    for i in reversed(range(len(subexpression))):
        add_sequence(subexpression[i])
    subexpression.clear()
    expression.clear()


def handle_cycle(expr_dict, expression):
    label = 'loop'
    end_label = 'end'
    program.append(label + ':')
    handle_expr_token(expr_dict['right']['left'], expression)
    next_next_left = expr_dict['left']['left']
    source_reg = None
    source_reg2 = None
    if next_next_left['type'] == 'SUBEXPRESSION':
        value = next_next_left['value']
        if value in jumps_dict.keys():
            if next_next_left['left']['left']['type'] == 'OPERAND_IDENT':
                source_reg = reg_var[next_next_left['left']['left']['value']]
            const_decl = next_next_left['right']['left']
            if const_decl['type'] == 'OPERAND_CONST':
                source_reg2 = free_registers.pop(0)
                seq = ['li', source_reg2, const_decl['value']]
                add_sequence(seq)
            jump_seq = [jumps_dict[value], source_reg2, source_reg, end_label]
            add_sequence(jump_seq)
            program.append("jal x0," + label)
            program.append(end_label + ":")

# TODO refactor!!!
def handle_expr_token(expr_dict, expression):
    if expr_dict['type'] == 'CYCLE_OPERATOR':
        handle_cycle(expr_dict, expression)
    if expr_dict['type'] == 'ASSIGNMENT':
        handle_assignment_token(expr_dict, expression)
    if expr_dict['type'] == 'SUBEXPRESSION':
        if 'value' in expr_dict:
            source_reg = None
            is_ident = False
            next_next_left = expr_dict['left']['left']
            if next_next_left['type'] == 'OPERAND_IDENT':
                reg = next_next_left['value']
                if reg in reg_var.keys():
                    is_ident = True
                    source_reg = reg_var[reg]
            if not is_ident:
                source_reg = free_registers.pop(0)
            handle_subexpression(expr_dict,
                                 rd=free_registers.pop(0),
                                 rs1=source_reg,
                                 rs2=free_registers.pop(0))
            expression.append(subexpression)
            for i in reversed(range(len(subexpression))):
                add_sequence(subexpression[i])
            subexpression.clear()
        else:
            if expr_dict['left']['type'] == 'OPERAND_CONST':
                reg = free_registers.pop(0)
                reg_var[assignments.pop()] = reg
                operation = ['li', reg, expr_dict['left']['value']]
                add_sequence(operation)
            elif expr_dict['left']['type'] == 'OPERAND_IDENT':
                val = expr_dict['left']['value']
                if val in reg_var.keys():
                    operation = ['add', free_registers.pop(0), 'x0', reg_var[val]]
                    add_sequence(operation)
            handle_expr_token(expr_dict['left'], expression)
    else:
        if expr_dict['type'] == 'EXPRESSION':
            pass
            handle_expr_token(expr_dict['left'], expression)


def handle_right_program_part(right_branch):
    pass
    if 'left' in right_branch:
        operation = right_branch['left']['type']
        if operation == 'OPERATOR':
            expression = []
            seq = right_branch['left']['left']
            if seq['type'] == 'ASSIGNMENT':
                # TODO check with vars list
                assignments.append(seq['value'])
            handle_expr_token(seq['left'], expression)
    if 'right' in right_branch:
        handle_right_program_part(right_branch['right'])  # handle next operators_list


def start_ast_handling(program_dict):
    handle_left_program_part(program_dict['left'])
    handle_right_program_part(program_dict['right'])


def read_yml(yml_file):
    with open(yml_file) as file:
        out_dict = yaml.load(file, Loader=yaml.FullLoader)
    return out_dict


ast_dict = read_yml(yml_file="test.yml")
start_ast_handling(ast_dict)
save_program()
