%{
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define YYERROR_VERBOSE 1
extern int yylex(void);
extern int yyparse();
extern FILE* yyin;
extern int yylineno;
void yyerror(char const *s);

#include <getopt.h>
%}

%code requires { 
	#include "node.h" 
}

%code {
	char *generateFile;
}

%union {
    char *string;
    Node *node;
}

%type <string> MINUS PLUS NOT MUL DIV LOWER GREATER EQUALS
%type <string> unary_operand
%type <string> IDENT CONST
%type <node> vars_declaration comp_description vars_list varibale operand complex_operator compound_operator
%type <node> programm operators_list operator assignment expression subexpression cycle_operator

%token MINUS PLUS NOT MUL DIV LOWER GREATER EQUALS 
%token VAR WHILE DO ASSIGN
%token DOT COMMA LB RB LCB RCB
%token IDENT CONST
%token OTHER

%left PLUS MINUS
%left MUL DIV
%left LOWER GREATER
%left EQUALS

%%

programm
: vars_declaration comp_description DOT	{
										$$ = node_create(PROGRAM, NULL, $1, $2);

										if (generateFile){
											FILE *gf = fopen(generateFile, "w");
											file_print_node(gf, $$);
											fclose(gf);
										}
										print_node($$);
										node_free($$);

										exit(0);
									}
;

comp_description
: LCB operators_list RCB 	{
								$$ = $2;
							}
;

vars_declaration
: VAR vars_list	{
					$$ = $2;
				}
;

vars_list
: varibale			{
						$$ = node_create(VARS_LIST, NULL, $1, NULL);
					}
| varibale COMMA vars_list  	{
									$$ = node_create(VARS_LIST, NULL, $1, $3);
								}
;

varibale
: IDENT		{
				$$ = node_create(VARIABLE, $1, NULL, NULL);
			}
;

operators_list
: operator			{
						$$ = node_create(OPERATORS_LIST, NULL, $1, NULL);
					}
| operator operators_list	{
								$$ = node_create(OPERATORS_LIST, NULL, $1, $2);
							}
;

operator
: assignment		{
						$$ = node_create(OPERATOR, NULL, $1, NULL);
					}
| complex_operator	{
						$$ = node_create(OPERATOR, NULL, $1, NULL);
					}
;

assignment
: IDENT ASSIGN expression 	{
								$$ = node_create(ASSIGNMENT, $1, $3, NULL);
							}
;

expression
: unary_operand subexpression	{
									$$ = node_create(EXPRESSION, $1, $2, NULL);
								}
| subexpression 				{
									$$ = node_create(EXPRESSION, NULL, $1, NULL);
								}
;

subexpression
: LB expression RB 								{
													$$ = node_create(SUBEXPRESSION, NULL, $2, NULL);
												}
| operand										{
													$$ = node_create(SUBEXPRESSION, NULL, $1, NULL);
												}
| subexpression MINUS subexpression	{
													$$ = node_create(SUBEXPRESSION, "\"-\"", $1, $3);
												}
| subexpression PLUS subexpression	{
	$$ = node_create(SUBEXPRESSION, "\"+\"", $1, $3);
}
| subexpression MUL subexpression	{
	$$ = node_create(SUBEXPRESSION, "\"*\"", $1, $3);
}
| subexpression DIV subexpression	{
	$$ = node_create(SUBEXPRESSION, "\"/\"", $1, $3);
}
| subexpression GREATER subexpression	{
	$$ = node_create(SUBEXPRESSION, "\">\"", $1, $3);
}
| subexpression LOWER subexpression	{
	$$ = node_create(SUBEXPRESSION, "\"<\"", $1, $3);
}
| subexpression EQUALS subexpression	{
	$$ = node_create(SUBEXPRESSION, "\"==\"", $1, $3);
	
	/*
	
	binary_operand
	: MINUS
	| PLUS
	| MUL
	| DIV
	| GREATER
	| LOWER
	| EQUALS
	;
	
	*/
}
;

unary_operand
: MINUS
| NOT
;



operand
: IDENT {
			$$ = node_create(OPERAND_IDENT, $1, NULL, NULL);
		}
| CONST {
			$$ = node_create(OPERAND_CONST, $1, NULL, NULL);
		}
;

complex_operator
	: cycle_operator	{
							$$ = node_create(COMPLEX_OPERATOR, NULL, $1, NULL);
						}
| compound_operator 	{
							$$ = node_create(COMPLEX_OPERATOR, NULL, $1, NULL);
						}
;

cycle_operator
: WHILE expression DO operator	{
									$$ = node_create(CYCLE_OPERATOR, NULL, $2, $4);
								}
;

compound_operator
: LCB operators_list RCB 	{
								$$ = node_create(COMPOUND_OPERATOR, NULL, $2, NULL);
							}
;

%%

int main(int argc, char **argv) {

	int c_opt;

	yyin = stdin;

	while (1){
		static struct option long_opt[] = {
	        {"help", 0, 0, 'h'},
	        {"generate", 1, 0, 'g'},
	        {"input", 1, 0, 'i'},
	        {0,0,0,0}
        };
		int optIdx;
		 
		if((c_opt = getopt_long(argc, argv, "g:i:h", long_opt, &optIdx)) == -1)
		  	break;

		switch(c_opt){
		    case 'h':
		        printf("USAGE: ./main -h -i filename -g filename \n\t-h (--help) for HELP\n\t-i (--input) filename to set input from file\n\t-g (--generate) filename to generate output to file. Using that opt will not kill the default output stream e.g console.\n");
		        exit(0);
		   
		    case 'g':
		        generateFile = optarg;
		        break;
		    case 'i': {
		    	FILE *fp = fopen(optarg, "r");
				if(fp){
					yyin = fp;
				} else {
					printf("can't open file: %s", optarg);				
				}
		    	break;    
			}				
		    default:
		        printf("TYPE ./main -h or --help to view full description of using options.\n");
		        break;
		  }
	}


	/* if (argc == 2){
		FILE *fp = fopen(argv[1], "r");
		if(fp){
			yyin = fp;
		} else {
			fprintf(stderr, "Error file opening!\n");
			return 1;
		}
	} else if(argc > 2) {
		printf("usage: [./main fileName], where fileName is optional\n");
		return 1;
	} else {
		yyin = stdin;
	}	*/
	
	yyparse();
    return 0;
}

void yyerror (char const *s) {
   fprintf (stderr, "got %s on line number %d\n", s, yylineno);
}
