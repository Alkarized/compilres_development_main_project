%{
#include <string.h>
#include "node.h"
#include "parser.tab.h"

void showError(char *token);
#define YYERROR_VERBOSE 1
%}

%%

"-"				{yylval.string = "-"; return MINUS;}
"+"				{yylval.string = "+"; return PLUS;}
"not"			{yylval.string = "not"; return NOT;}
"*"				{yylval.string = "*"; return MUL;}
"/"				{yylval.string = "/"; return DIV;}
"<"				{yylval.string = "<"; return LOWER;}
">"				{yylval.string = ">"; return GREATER;}
"=="			{yylval.string = "=="; return EQUALS;}

"="             {return ASSIGN;}

"Var"			{return VAR;}
"WHILE" 		{return WHILE;}
"DO"			{return DO;}

"."				{return DOT;}
","				{return COMMA;}
"("         	{return LB;}
")"         	{return RB;}
"["             {return LCB;}
"]"             {return RCB;}



[a-z]+          {yylval.string = strdup(yytext); return IDENT; }
[0-9]+          { yylval.string = strdup(yytext); return CONST; }


[ \t\n]          // Skip whitespace, tabs, and newlines

[{].*[}]       	 // Skip comments

.                {showError(yytext); return (OTHER);}

%%

int yywrap() {
    return 1;
}

void showError(char* token) {
    printf("Unknown token: %s\n", token);
}
